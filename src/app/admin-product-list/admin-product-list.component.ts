import { Component, OnInit, ViewChild } from '@angular/core';
import { DbServiceService } from "../db-service.service";
import { Product } from '../product';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';

@Component({
  selector: 'app-admin-product-list',
  templateUrl: './admin-product-list.component.html',
  styleUrls: ['./admin-product-list.component.css']
})
export class AdminProductListComponent implements OnInit {
dataSource: Product[] = [];
displayedColumns: string[] = ['_id', 'Name', 'action'];
@ViewChild(MatTable,{static:true}) table: MatTable<any>;
  constructor(private DbServiceService: DbServiceService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.DbServiceService.getProductData()
    .subscribe((Product) => {this.dataSource = Product;
    });
  }

  openDialog(action:any,obj:any) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Add'){
        //this.addRowData(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }

  /*addRowData(row_obj:any){
    var d = new Date();
    this.dataSource.push({
      _id:d.getTime(),
      Name:row_obj.name
    });
    this.table.renderRows();
    
  }*/
  updateRowData(row_obj:any){
    this.dataSource = this.dataSource.filter((value,key)=>{
      if(value._id == row_obj.id){
        value.name = row_obj.name;
      }
      return true;
    });
  }
  deleteRowData(row_obj:any){
    this.dataSource = this.dataSource.filter((value,key)=>{
      return value._id != row_obj.id;
    });
  }

}
