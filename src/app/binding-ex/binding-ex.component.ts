import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding-ex',
  templateUrl: './binding-ex.component.html',
  styleUrls: ['./binding-ex.component.css']
})
export class BindingExComponent implements OnInit {

   buttonStatus = true;
cssStrng:string
flag=true;
texterror = "texterror";
fontStyle = "fontStyle";
classBinding = {
  'text-error': true,
  'font-style':true
}

titleStyle = 'green';
  titleStyle_exist = true;
  titleStyles = {
    'color':'green',
    'font-size':'4em'
  }
  constructor() { }

  ngOnInit(): void {
    this.cssStrng = "red size20";
  }

  

getClass() {
  return 'red';
}

   myEvent(event:any){
     console.log(event);
   }

}
