import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoComponent } from './todo/todo.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { AdminProductListComponent } from './admin-product-list/admin-product-list.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { AuthenticationGuard } from './authentication.guard';
import { CartDetailsComponent } from './cart-details/cart-details.component';

const routes: Routes = [
   {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path:'todo', component:TodoComponent},
  {path:'product-list', component:ProductListComponent},
  {path:'product-detail/:id', component:ProductDetailComponent},
   {path:'login', component:LoginFormComponent},
    {path:'register', component:RegistrationFormComponent},
    {path:'admin-dashboard', component:AdminhomeComponent},
     {path:'Employee-details', component:EmployeeDetailsComponent,
    canActivate:[AuthenticationGuard]},
     {path:'admin-product-list',component: AdminProductListComponent},
      {path:'cart-details', component:CartDetailsComponent},
      {path: '**', component: LoginFormComponent}
     
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
