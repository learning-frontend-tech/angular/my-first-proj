import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router,  } from '@angular/router';
import { DbServiceService } from "../db-service.service";
import { Product } from '../product';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {BehaviourSubjectService} from '../behaviour-subject.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
products:Product[] = [];
isLinkDisabled = false;
errorMessage:string;
  constructor(private DbServiceService:DbServiceService, private Router: Router,
  private BehaviourSubjectService:BehaviourSubjectService) { }

  ngOnInit(): void {
    
    
    this.DbServiceService.getProductData().subscribe(
      (response:any) => this.products = response,
      (error) => alert(error)
    );

  }

  addToCart(product){

    if(!localStorage.getItem("LoggedinUserId"))
    {
       Swal.fire({
  icon: 'error',
  title: 'Not Logged In',
  text: 'You are not Logged In. First Login with your credential!' 
})

this.Router.navigate(['/login']);     

    }
    else {   
      this.isLinkDisabled = true;        
      this.DbServiceService.addItemsToCart(product)
    .subscribe((response) => {
      console.log(response);
      Swal.fire({
  icon: 'success',
  title: 'Product added to cart',
  text:''  
}) 
this.isLinkDisabled = false;

})
    }

  }

}
