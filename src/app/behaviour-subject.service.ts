import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';  

@Injectable({
  providedIn: 'root'
})
export class BehaviourSubjectService {
public User = new BehaviorSubject<boolean>(false);
TelecastUser = this.User.asObservable();

public isLoggedIn = new BehaviorSubject<boolean>(false);
TelecastisLoggedIn = this.isLoggedIn.asObservable();


public LoggedInUser = new BehaviorSubject<string>('');
TelecastLoggedInUser = this.LoggedInUser.asObservable();

public totalItems = new BehaviorSubject<number>(0);
TelecasttotalItems = this.totalItems.asObservable();

//public cartItemTotalPrice = new BehaviorSubject<number>(0);
//TelecastcartItemTotalPrices = this.cartItemTotalPrice.asObservable();
 constructor() { }

setUser(isAdmin: boolean) {  
  
    this.User.next(isAdmin); //it is publishing this value to all the subscribers that have already subscribed to this message
}

setIsLoggedIn(isLoggedIn){
  this.isLoggedIn.next(isLoggedIn);
}

setUserName(LoggedInUser:string){
  this.LoggedInUser.next(LoggedInUser);
}

settotalItems(totalItems:number){
  this.totalItems.next(totalItems);
}
/*
setcartItemTotalPrice(cartItemTotalPrice:number){
  this.cartItemTotalPrice.next(cartItemTotalPrice);
}*/
 
}
