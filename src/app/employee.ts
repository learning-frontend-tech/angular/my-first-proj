export class Employee {
    _id;
    Name: string;
    Department: string;
    Phone: number;
    isEditable:boolean;

    constructor(_id,Name: string,
    Department: string,
    Phone: number,
    isEditable:boolean){
        this._id = _id;
        this.Name = Name;
        this.Department = Department;
        this.Phone = Phone;
        this.isEditable = false;
    }
}
