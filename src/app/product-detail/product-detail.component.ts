import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
id:number;

  constructor(private act1: ActivatedRoute, private route: Router) { }
//ActivatedRoute obj = new ActivatedRoute("ABC","XEF");

  ngOnInit(): void {
    this.id = this.act1.snapshot.params.id;
  
  }

  goToList(){
    this.route.navigate(['/product-list']);
  }

}
