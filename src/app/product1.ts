export class Product1 {
    id: number;
    name: String;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}
