import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LightBulbComponent } from './light-bulb/light-bulb.component';
import { ChangeBackgrndComponent } from './change-backgrnd/change-backgrnd.component';
import { TodoComponent } from './todo/todo.component';
import { BindingExComponent } from './binding-ex/binding-ex.component';
import { DirectivesExComponent } from './directives-ex/directives-ex.component';
import { ProductList1Component } from './product-list1/product-list1.component';
import { ProductDetail1Component } from './product-detail1/product-detail1.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';

import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { HeaderComponent } from './header/header.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';


import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AdminProductListComponent } from './admin-product-list/admin-product-list.component';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminNavBarComponent } from './admin-nav-bar/admin-nav-bar.component';
import { UserNavBarComponent } from './user-nav-bar/user-nav-bar.component';

import { AuthGuardServiceService } from './auth-guard-service.service';
import { CartDetailsComponent } from './cart-details/cart-details.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LightBulbComponent,
    ChangeBackgrndComponent,
    TodoComponent,
    BindingExComponent,
    DirectivesExComponent,
    ProductList1Component,
    ProductDetail1Component,
    ProductListComponent,
    ProductDetailComponent,
    LoginFormComponent,
    RegistrationFormComponent,
    HeaderComponent,
    AdminhomeComponent,
    AdminProductListComponent,
    DialogBoxComponent,
    EmployeeDetailsComponent,
    AdminNavBarComponent,
    UserNavBarComponent,
    CartDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    MatDialogModule,
    MatFormFieldModule,
    MatTableModule,
    MatInputModule,
    NgbModule
  ],
  providers: [AuthGuardServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
