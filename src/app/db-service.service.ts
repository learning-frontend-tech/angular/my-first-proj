import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from './product';
import {CartResponse} from './cart-response';
import { Observable } from 'rxjs'; 
import { Employee } from './employee';
import { retry, catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DbServiceService {

  constructor(private http:HttpClient) { }
  apiUrl =  'http://localhost:3000/api';

getData(){
  alert("data");
}

getProductData(): Observable<Product[]>{
  return this.http.get<Product[]>(this.apiUrl + '/getProduct')
  .pipe(catchError((err) =>{
    console.log(err);
     return throwError(err); 
  }));
}

getTodoData(){
  return this.http.get(this.apiUrl + '/getTodo');

}

checkLoginFromServer(email:string,password:string){
  return this.http.post(this.apiUrl + '/check-login', {email, password})
  .pipe(catchError((err) =>{
    console.log(err);
     return throwError(err); 
  }))

}

createAccountInDB(model:any){
  model.isAdmin="false";
  return this.http.post(this.apiUrl + '/create-account', {model}, {responseType:'text'});
}

deleteTodoTask(id:any){
  return this.http.delete(this.apiUrl + '/deleteTodo/' + id);
}

updateTodoTask(todo:any){
  return this.http.put(this.apiUrl + '/updateTodo' + todo._id,{todo});
}

getEmployeeData(){
  return this.http.get<Employee[]>(this.apiUrl + '/getEmployeeData');
}
updateEmployee(employee:Employee){
  let newEmp = {Name: employee.Name, 
    Department:employee.Department, 
    Phone:employee.Phone};
  return this.http.put<Employee[]>(this.apiUrl + '/updateEmployee/' + employee._id, {newEmp})
}

createEmployee(employee){
  return this.http.post<Employee[]>(this.apiUrl + '/createEmployee', {employee});
}

deleteEmployee(employee){
  return this.http.delete<Employee[]>(this.apiUrl + '/deleteEmployee/' + employee._id)

}

addItemsToCart(product:Product){
  let customerId = localStorage.getItem("LoggedinUserId");
  console.log(product);
  let addedProduct = {_id:product._id,name:product.name, quantity:1, price:product.price}

  return this.http.post(this.apiUrl + '/add-items-to-cart', {customerId, addedProduct},{responseType:'text'})
}

getCartDetailsfromServer(){
  let customerId = localStorage.getItem("LoggedinUserId");
  return this.http.get<CartResponse>(this.apiUrl + "/get-cart-details/" + customerId);
  
}

getProductDataById(productId){
  return this.http.get<Product[]>(this.apiUrl + '/getProductDataById/' + productId);

}

clearCartDataFromServer(){
   let customerId = localStorage.getItem("LoggedinUserId");
  return this.http.delete(this.apiUrl + '/clearCart/' + customerId,{responseType:'text'});

}

removeCartItem(product:Product){
   let customerId = localStorage.getItem("LoggedinUserId");
   let productToDel = product;
  return this.http.post(this.apiUrl + '/delete-item',{customerId, productToDel},{responseType:'text'});  
}

deleteFromCart(product:Product){
   let customerId = localStorage.getItem("LoggedinUserId");
   let productToDecrement = product;

   return this.http.post(this.apiUrl + '/decrease-item',{customerId, productToDecrement},{responseType:'text'});  

}

}

