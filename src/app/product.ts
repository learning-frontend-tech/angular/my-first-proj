export class Product {
    _id: number;
    name: string;
    category:string;
    price: number;
    quantity:number;
    type:string;
    imgPath:string;

    constructor( id: number,
    name: string,
    category:string,
    price: number,
    quantity:number,
    type:string,
    imgPath:string){
        this._id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.quantity = quantity;
        this.type = type;
        this.imgPath = imgPath;
    }

}
