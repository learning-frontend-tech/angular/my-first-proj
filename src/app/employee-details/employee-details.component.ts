import { Component, OnInit } from '@angular/core';
import { DbServiceService } from "../db-service.service";
import { Employee } from '../employee';


@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
employees:Employee[] = [];
empModel:any = {};
isEditable:boolean = false;
isAddedNew:boolean = false;
  constructor(private DbServiceService: DbServiceService) { }

  ngOnInit(): void {    

    this.DbServiceService.getEmployeeData()
    .subscribe(response => {this.employees = response;
      console.log(this.employees)});
  }

  setEditState(employee, state){
    employee.isEditable = state;  
    
  }

  update(employee){
    this.DbServiceService.updateEmployee(employee)
    .subscribe((response) =>{this.employees = response;
    })
  }

  delete(employee){
    this.DbServiceService.deleteEmployee(employee)
    .subscribe((response) =>{this.employees = response;
    })

  }

  createNew(){
    this.isAddedNew = true;
    this.empModel = {};

  }

  create(){
    this.DbServiceService.createEmployee(this.empModel)
    .subscribe(Employee => {this.employees = Employee;
      this.isAddedNew = false})
  }

  setAddedState(){
    this.isAddedNew = false;
    this.empModel = {};
  }

}
