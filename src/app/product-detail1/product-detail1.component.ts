import { Component, OnInit } from '@angular/core';
import { Product1 } from "../product1";
import { Router, ActivatedRoute } from '@angular/router'; 

@Component({
  selector: 'app-product-detail1',
  templateUrl: './product-detail1.component.html',
  styleUrls: ['./product-detail1.component.css']
})
export class ProductDetail1Component implements OnInit {
product: any;
   id: number;

  constructor(private router : Router, private route : ActivatedRoute ) { 
     this.id = this.route.snapshot.params.id;
  }
 
  ngOnInit(): void {
     
  }

     goToList() { 
      this.router.navigate(['/product-list']); 
   } 

}
