import { Component, OnInit } from '@angular/core';
import { Product1 } from "../product1";

@Component({
  selector: 'app-product-list1',
  templateUrl: './product-list1.component.html',
  styleUrls: ['./product-list1.component.css']
})
export class ProductList1Component implements OnInit {
Products:Product1[] = [];
  constructor() { }

  ngOnInit(): void {
    this.Products =  [
      {  id:  1,  name:  'Product 1' },
      {  id:  2,  name:  'Product 2' },
      {  id:  3,  name:  'Product 3' },
      {  id:  4,  name:  'Product 4' },
      {  id:  5,  name:  'Product 5' },
       {  id:  6, name:  'Product 6' }          
    ];
  }

}
