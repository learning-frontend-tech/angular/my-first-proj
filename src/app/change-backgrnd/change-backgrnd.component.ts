import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-change-backgrnd',
  templateUrl: './change-backgrnd.component.html',
  styleUrls: ['./change-backgrnd.component.css']
})
export class ChangeBackgrndComponent implements OnInit {
imgSrc:string;
imgSrcArray:any = [];

  constructor() { 
    this.imgSrcArray = ["../assets/images/background1.jpg", 
    "../assets/images/background2.jpg",
    "../assets/images/background3.jpg"];   
     
  }

  ngOnInit(): void {
     this.imgSrc = this.imgSrcArray[0];
    let i=1;
   setInterval(() => {
     
     this.imgSrc = this.imgSrcArray[i];
     i++;   

     if(i==3)
      i = 0;

     
  }, 3000);
  }

}
