import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeBackgrndComponent } from './change-backgrnd.component';

describe('ChangeBackgrndComponent', () => {
  let component: ChangeBackgrndComponent;
  let fixture: ComponentFixture<ChangeBackgrndComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeBackgrndComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeBackgrndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
