import { Component, OnInit } from '@angular/core';
import { DbServiceService } from "../db-service.service";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
items:any = [];
newTask:string;
condition:boolean = true;
isEditMode: boolean = false;
  constructor(private DbServiceService:DbServiceService) { }

  ngOnInit(): void {
    //this.items[0] = "buy milk";
    //this.items[1] = "Electricity Bill";
    //this.items[2] = "Doctor's Appointment";
   // this.items[3] = "Study Angular at home";

       this.DbServiceService.getTodoData().subscribe(
      (response:any) => {this.items = response;
      console.log(this.items)},
      (error) => console.log(error)
    );
    
  }

  addToList(){
    if(this.newTask == ''){

    }
    else{
      this.items.push(this.newTask);
      this.newTask = '';
    }

  }

  EditTask(item:any){

  }

  deleteTask(item:any){   
    
    this.DbServiceService.deleteTodoTask(item._id)
    .subscribe((data:any) => {
      console.log(data);
                for(var i=0;i<this.items.length;i++){
            if(this.items[i].text == item.text){
              this.items.splice(i,1);
            }
          }
    })
    

  }

}
