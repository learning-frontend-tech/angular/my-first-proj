import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {BehaviourSubjectService} from './behaviour-subject.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
 
  title = 'My First Angular Project';
  sidenav:any;

  data:string = "Data to be displayed";
  applied = true;
  isAdmin:boolean = false;
  Flag:any;

  constructor(private Router:Router, private BehaviourSubjectService:BehaviourSubjectService){}

ngOnInit(){
   this.BehaviourSubjectService.TelecastUser.subscribe((isAdmin) => {        
       console.log(isAdmin);
       this.isAdmin = isAdmin;
      });

  this.Flag = localStorage.getItem("isAdmin");

  if(this.Flag == "true"){
    this.isAdmin = true;
    //this.Router.navigate(['/admin-dashboard'])
  }
  else{
    this.isAdmin = false;
  }
     
}
  


}
