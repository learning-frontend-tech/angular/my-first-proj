import { Component, OnInit } from '@angular/core';
import { DbServiceService } from "../db-service.service";
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {
model:any = {};
  constructor(private DbServiceService:DbServiceService, private Router:Router) { }

  ngOnInit(): void {
    $(document).ready(function($:any){
  $('#birth-date').mask('00/00/0000');
  $('#phone-number').mask('0000-0000');
 })
  }

  createAccount(){
    this.DbServiceService.createAccountInDB(this.model)
    .subscribe((response) => {
      if(response == "success"){
        this.Router.navigate(['/login']);

      }
      else{

      }
    })
  }

}
