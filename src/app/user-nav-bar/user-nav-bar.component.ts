import { Component, OnInit } from '@angular/core';
import {BehaviourSubjectService} from '../behaviour-subject.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-nav-bar',
  templateUrl: './user-nav-bar.component.html',
  styleUrls: ['./user-nav-bar.component.css']
})
export class UserNavBarComponent implements OnInit {
isLoggedIn:boolean;
LoggedInUser:string = "";

  constructor(private BehaviourSubjectService: BehaviourSubjectService,
  private Router: Router) { }

  ngOnInit(): void {
  
    this.BehaviourSubjectService.TelecastisLoggedIn.subscribe((isLoggedIn) =>
    this.isLoggedIn = isLoggedIn);

      if(localStorage.getItem("isLoggedIn") == "true")
      this.isLoggedIn = true;

    this.BehaviourSubjectService.TelecastLoggedInUser.subscribe((LoggedInUser) =>
    this.LoggedInUser = LoggedInUser);
     this.LoggedInUser =  localStorage.getItem("LoggedInUser"); 
     
  }

  logout(){
    localStorage.removeItem("LoggedInUser");
    localStorage.removeItem("isAdmin");
    localStorage.removeItem("LoggedinUserId");
    localStorage.removeItem("isLoggedIn");
    this.BehaviourSubjectService.setIsLoggedIn(false);
    this.BehaviourSubjectService.setUserName("");
     this.BehaviourSubjectService.setUser(false);
    this.Router.navigate(['/login']);

  }

}
