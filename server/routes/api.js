const express = require('express');
var mongojs  = require('mongojs');
var bodyParser = require('body-parser');

var db = mongojs('mongodb://localhost:27017/test',['todos', 'users', 'employee', 'products','cart']);

const router = express.Router();

router.get('/getProduct', (req,res)=>{
    console.log('request received');
    db.products.find(function(err, products){
        if(err || !products){
            res.status(404).send('Not Found');  
        }
        else{
            res.send(products);
        }
    })

    
});

router.get('/getProductDataById/:id',(req,res)=>{
    db.products.findOne({_id:mongojs.ObjectId(req.params.id)}, (err, result)=>{
        if(err || !result){
            res.status(404).send("Product Data not found");
        }
        else{
            res.send(result);
        }

    })
})

router.get('/getTodo',function(req,res,next){
    console.log("request received");
    db.todos.find(function(err,todos){
        if(err){
            console.log(err);
            res.send(err);
            
        }
        else{
            res.send(todos);
            //console.log(todos);
        }
    })
});

router.post('/check-login', (req, res) =>{
    var email = req.body.email;
    console.log(email);
    var password = req.body.password;

    db.users.findOne({email: req.body.email, password: req.body.password},(err, user) =>{
        if(err || !user){     
           
            res.status(404).send('Not Found');                  
        }
        else{            
              res.send(user);
        }
    })
})

router.post('/create-account', (req,res) =>{
    console.log(req.body.model.username);
    db.users.save(req.body.model, (err, result)=>{
        if(err)
            res.send(err);
        else{
            console.log(result);
            res.send("success");
        }
    })

});


router.delete('/deleteTodo/:id',function(req,res,next){
    db.todos.remove({
        _id:mongojs.ObjectId(req.params.id)
    },'',function(err,result){
        if(err){
            res.send(err)
        }
        else{
            res.json(result);
        }
    });
});


router.put('/updateTodo/:id', (req,res) => {
    var todo = req.body.todo

    db.todos.updateOne({_id:mongojs.ObjectId(req.params.id)}, {$set:{text:todo}},
    (err, result)=>{
        if(err){
            res.send(err);
        }

        else{
            res.json(result);
        }
    })
})

router.get('/getEmployeeData', (req,res) =>{
    db.employee.find((err, employees) =>{
        if(err)
            res.send(err);
        else
            res.send(employees)
    })

});

router.put('/updateEmployee/:id', (req,res) =>{
    console.log(req.body.newEmp);
    var updtObj = req.body.newEmp;

    var newvalues = {
   $set: updtObj
}
    db.employee.updateOne({_id:mongojs.ObjectId(req.params.id)}, newvalues, (err, result) =>{
        if(err){
            res.send(err);
        }
        else{
             db.employee.find((err, employees) =>{
             if(err)
                 res.send(err);
            else
                res.send(employees)
            })
        }
    })
})

router.post('/createEmployee', (req,res)=>{
    db.employee.save(req.body.employee, (err, result)=>{
        if(err)
            res.send(err);
        else{
            db.employee.find((err, employees) =>{
             if(err)
                 res.send(err);
            else
                res.send(employees)
            })
            
        }
    })

})

router.delete('/deleteEmployee/:id', (req,res) =>{
     db.employee.remove({
        _id:mongojs.ObjectId(req.params.id)
    },'',function(err,result){
        if(err){
            res.send(err)
        }
        else{            
            db.employee.find((err, employees) =>{
             if(err || !employees)
                 res.send(err);
            else
                res.send(employees)
            })
        }
    });

})

router.post('/add-items-to-cart', (req,res) =>{
    //console.log(req.body.addedProduct);
    //console.log(req.body.customerId);

    let cartPrice = 0;

    db.cart.findOne({_id:mongojs.ObjectId(req.body.customerId)},(err, cartData) =>{
        if(err || !cartData){
            console.log("not found")
            cartPrice = 0;

    db.cart.update(
    { _id: mongojs.ObjectId(req.body.customerId)}
  , {
      $set: { totalPrice: cartPrice + req.body.addedProduct.price }
    , $push: { products: req.body.addedProduct        
    }
  }, {upsert:true, new:true}, function(err, success){
       if (err) {
        console.log(err);
         res.status(500).send("Error");
        } else {
            res.status(200).send("cart updated");
        }
  });

        }
        else{          
            console.log("found"); 
            cartPrice = cartData.totalPrice;     


var newQuantity = 0;
var updateFlag=false;
for(var i = 0; i < cartData.products.length; i++) {
  if(cartData.products[i]._id == req.body.addedProduct._id) {
    newQuantity = cartData.products[i].quantity + 1;
    console.log("req.body.addedProduct.quantity" + newQuantity)
updateFlag = true;
    break;

  }
  else{
      continue;
  }}
  
if(updateFlag == true){
    console.log("inside update");
  db.cart.update( { _id: mongojs.ObjectId(req.body.customerId),
 "products._id": req.body.addedProduct._id},
{$set:{totalPrice: cartPrice + req.body.addedProduct.price,
 "products.$.quantity":  newQuantity}
}, (err,result) =>{
    if(err){
        console.log(err);
        res.status(500).send("Error");
    }

    else{
        res.status(200).send("cart updated");
        console.log(result);
    }
})
}
else if(updateFlag == false){
      console.log("inside new");
    db.cart.update(
    { _id: mongojs.ObjectId(req.body.customerId),
    }
  , {
      $set: { totalPrice: cartPrice + req.body.addedProduct.price }
    , $push: { products: req.body.addedProduct        
    }
  }, {}, function(err, success){
       if (err) {
        console.log(err);
         res.status(500).send("Error");

    } else {
         res.status(200).send("cart added");
        console.log(success);
            //console.log(success)
        }
  });
}
            



        }
    })
   
   

}
)

router.get('/get-cart-details/:customerId' , (req,res)=>{
    db.cart.find({_id:mongojs.ObjectId(req.params.customerId)}, (err,result) =>{
        if(err || !result){
            res.status(404).send("Cart data not found");
        }
        else{
            res.send(result);
        }

    })
});

router.delete('/clearCart/:customerId', (req,res) =>{
    console.log("clear cart called");
     db.cart.remove({
        _id:mongojs.ObjectId(req.params.customerId)
    },'',function(err,result){
        if(err){
            res.send(err);
            res.status(500).send("Error");
        }
        else{
           res.status(200).send("Cart Data Deleted");
        }
    });

});

router.post('/delete-item' ,(req,res) =>{
     let cartPrice = 0;

    db.cart.findOne({_id:mongojs.ObjectId(req.body.customerId)},(err, cartData) =>{
        if(err){

        }

        else{
             cartPrice = cartData.totalPrice;    
             let oldQuantity = 0;

             for(var i = 0; i < cartData.products.length; i++) {
  if(cartData.products[i]._id == req.body.productToDel._id) {
    oldQuantity = cartData.products[i].quantity;
  }
}


             db.cart.update(
    { _id: mongojs.ObjectId(req.body.customerId),
    }
  , {
      $set: { totalPrice: cartPrice - (req.body.productToDel.price * oldQuantity) }
    , $pull: { "products": { _id:req.body.productToDel._id}
    }
  }, {}, function(err, success){
       if (err) {
        console.log(err);
         res.status(500).send("Error");

    } else {
         res.status(200).send("cart item removed");
        console.log("success");
            //console.log(success)
        }
  }); 
        }



    })


})

router.post('/decrease-item', (req,res) =>{

     let cartPrice = 0;

    db.cart.findOne({_id:mongojs.ObjectId(req.body.customerId)},(err, cartData) =>{
        if(err || !cartData){

        }
        else{          
            console.log("found"); 
            cartPrice = cartData.totalPrice;     


let newQuantity = 0;
var updateFlag=false;
for(var i = 0; i < cartData.products.length; i++) {
  if(cartData.products[i]._id == req.body.productToDecrement._id) {
    newQuantity = cartData.products[i].quantity - 1;
    console.log("req.body.productToDecrement.quantity" + newQuantity)
updateFlag = true;
    break;

  }
  else{
      continue;
  }}
  
if(updateFlag == true){
    console.log("inside update");
  db.cart.update( { _id: mongojs.ObjectId(req.body.customerId),
 "products._id": req.body.productToDecrement._id},
{$set:{totalPrice: cartPrice - req.body.productToDecrement.price,
 "products.$.quantity":  newQuantity}
}, (err,result) =>{
    if(err){
        console.log(err);
        res.status(500).send("Error");
    }

    else{
        res.status(200).send("cart updated");
        console.log(result);
    }
})
}
        }
    })

})

module.exports = router;

