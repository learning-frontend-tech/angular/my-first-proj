const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();
const api = require('./server/routes/api');

// parse application/json
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false}));


app.use(express.static(path.join(__dirname, 'dist/my-first-proj')));

app.use('/api', api);

app.get('*', function(req, res){
    res.sendFile(path.join(__dirname,'dist/my-first-proj/index.html'));
})
app.listen(3000,()=>{
    console.log("server is listening at port 3000");
});



